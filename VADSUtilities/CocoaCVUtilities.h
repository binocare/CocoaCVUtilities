//
//  CocoaCVUtilities.h
//  VADSDelegates
//
//  Created by JiangZhiping on 16/3/26.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#define DEG_TO_RAD (CV_PI/180.0)
#define RAD_TO_DEG (180.0/CV_PI)

#import <CocoaXtensions/CocoaXtensions.h>
#import <CocoaUtilities/CocoaUtilities.h>
#import <CocoaCVUtilities/CIImage+OpenCVMat.h>
#import <CocoaCVUtilities/TypeConversions.h>
#import <CocoaCVUtilities/NSValue+CVFormats.h>
#import <CocoaCVUtilities/NSArray+vectorOfCVFormats.h>

#if TARGET_OS_IPHONE
#import <CocoaCVUtilities/CMAttitude+OpenCVMat.h>
#endif

//#import <CocoaCVUtilities/CvVectorGeometry.h>
#import <CocoaCVUtilities/VADSKalmanFilter.h>
#import <CocoaCVUtilities/VADSMedianMeanSmoother.h>
#import <CocoaCVUtilities/VADSUnwrapper.h>

#if !TARGET_OS_IPHONE
#import <CocoaCVUtilities/TileWindow.h>
#import <CocoaCVUtilities/TileWindowManager.h>
#endif

//
//  TileWindowManager.h
//  TestNSWindows
//
//  Created by JiangZhiping on 16/3/31.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <CocoaXtensions/CocoaXtensions.h>
#import "TileWindow.h"

@interface TileWindowManager : NSObject

@property NSMutableDictionary<NSString *, TileWindow *> * windowDictionary;

+ (instancetype) getInstance;

- (void) addWindowTitledWith:(NSString *) uniqueWindowTitle;

- (void) addWindowTitledWith:(NSString *) uniqueWindowTitle AndColor:(NSColor *)color;

- (TileWindow *) getWindowTitledWith:(NSString *)windowTitle;

- (void) moveWindowNamed:(NSString *) windowTitle ToPosition:(NSPoint)point;

- (void) removeWindowTitledWith:(NSString *) windowTitle;

- (void) removeAllWindow;

@end

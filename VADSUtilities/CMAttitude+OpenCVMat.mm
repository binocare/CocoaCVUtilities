//
//  CMAttitude+OpenCVMat.m
//  CocoaCVUtilities
//
//  Created by JiangZhping on 2016/9/30.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CMAttitude+OpenCVMat.h"

@implementation CMAttitude (OpenCVMat)

@dynamic cvRotationMatrix;

- (cv::Matx33d) cvRotationMatrix {
    CMRotationMatrix rm = self.rotationMatrix;
    return cv::Matx33d(rm.m11, rm.m12, rm.m13, rm.m21, rm.m22, rm.m23, rm.m31, rm.m32, rm.m33);
}

@end

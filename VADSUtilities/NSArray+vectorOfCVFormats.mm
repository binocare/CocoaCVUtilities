//
//  NSArray+vectorOfCVFormats.m
//  CocoaCVUtilities
//
//  Created by JiangZhping on 16/7/30.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "NSArray+vectorOfCVFormats.h"
#import "NSValue+CVFormats.h"

@implementation NSArray (vectorOfCVFormats)

+ (NSArray<NSValue *> *) arrayWithCvPoint2iVector:(vector<Point2i>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvPoint2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvPoint2fVector:(vector<Point2f>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvPoint2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvPoint2dVector:(vector<Point2d>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvPoint2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvPoint3iVector:(vector<Point3i>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvPoint3d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvPoint3fVector:(vector<Point3f>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvPoint3d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvPoint3dVector:(vector<Point3d>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvPoint3d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvSize2iVector:(vector<Size2i>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvSize2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvSize2fVector:(vector<Size2f>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvSize2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvSize2dVector:(vector<Size2d>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvSize2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvRect2iVector:(vector<Rect2i>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvRect2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvRect2fVector:(vector<Rect2f>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvRect2d:v[i]]];
    }
    return array;
}
+ (NSArray<NSValue *> *) arrayWithCvRect2dVector:(vector<Rect2d>)v {
    NSMutableArray<NSValue *> * array = [NSMutableArray arrayWithCapacity:v.size()];
    for (int i = 0; i < v.size() ; i ++) {
        [array addObject:[NSValue valueWithCvRect2d:v[i]]];
    }
    return array;
}

- (vector<Point2i>) CvPoint2iVector {
    vector<Point2i> list;
    for (NSValue * v in self) {
        list.push_back([v cvPoint2iValue]);
    }
    return list;
}
- (vector<Point2f>) CvPoint2fVector {
    vector<Point2f> list;
    for (NSValue * v in self) {
        list.push_back([v cvPoint2fValue]);
    }
    return list;
}
- (vector<Point2d>) CvPoint2dVector {
    vector<Point2d> list;
    for (NSValue * v in self) {
        list.push_back([v cvPoint2dValue]);
    }
    return list;
}
- (vector<Point3i>) CvPoint3iVector {
    vector<Point3i> list;
    for (NSValue * v in self) {
        list.push_back([v cvPoint3iValue]);
    }
    return list;
}
- (vector<Point3f>) CvPoint3fVector {
    vector<Point3f> list;
    for (NSValue * v in self) {
        list.push_back([v cvPoint3fValue]);
    }
    return list;
}
- (vector<Point3d>) CvPoint3dVector {
    vector<Point3d> list;
    for (NSValue * v in self) {
        list.push_back([v cvPoint3dValue]);
    }
    return list;
}
- (vector<Size2i>) CvSize2iVector {
    vector<Size2i> list;
    for (NSValue * v in self) {
        list.push_back([v cvSize2iValue]);
    }
    return list;
}
- (vector<Size2f>) CvSize2fVector {
    vector<Size2f> list;
    for (NSValue * v in self) {
        list.push_back([v cvSize2fValue]);
    }
    return list;
}
- (vector<Size2d>) CvSize2dVector {
    vector<Size2d> list;
    for (NSValue * v in self) {
        list.push_back([v cvSize2dValue]);
    }
    return list;
}
- (vector<Rect2i>) CvRect2iVector {
    vector<Rect2i> list;
    for (NSValue * v in self) {
        list.push_back([v cvRect2iValue]);
    }
    return list;
}
- (vector<Rect2f>) CvRect2fVector {
    vector<Rect2f> list;
    for (NSValue * v in self) {
        list.push_back([v cvRect2fValue]);
    }
    return list;
}
- (vector<Rect2d>) CvRect2dVector {
    vector<Rect2d> list;
    for (NSValue * v in self) {
        list.push_back([v cvRect2dValue]);
    }
    return list;
}

@end

//
//  VADSUnwrapper.m
//  VADSDelegates
//
//  Created by JiangZhiping on 16/4/2.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "VADSUnwrapper.h"

@implementation VADSUnwrapper

+ (cv::Mat1d) unwrap:(cv::Mat1d &) wrappedVectorMat {
    static cv::Mat1d previousVectorMat;
    
    // in case of empty
    if (previousVectorMat.empty() == YES) {
        wrappedVectorMat.copyTo(previousVectorMat);
    }
    // in case of vector length changed.
    if (wrappedVectorMat.rows != previousVectorMat.rows ) {
        wrappedVectorMat.copyTo(previousVectorMat);
    }
    
    cv::Mat1d gap = wrappedVectorMat - previousVectorMat;
    cv::Mat inRangeResult;
    cv::inRange(gap, -CV_PI, CV_PI, inRangeResult);
    cv::bitwise_not(inRangeResult, inRangeResult);
    std::vector<cv::Point> outlierIndex;
    cv::findNonZero(inRangeResult, outlierIndex);
    
    for (int i = 0 ; i < outlierIndex.size(); i ++) {
        int x = outlierIndex[i].x;
        int y = outlierIndex[i].y;
        
        if (gap(y,x) >= CV_PI) {
            while (gap(y,x) >= CV_PI || gap(y,x) <= -CV_PI) {
                wrappedVectorMat(y,x) -= CV_2PI;
                gap(y,x) -= CV_2PI;
            }
        } else {
            while (gap(y,x) >= CV_PI || gap(y,x) <= -CV_PI) {
                wrappedVectorMat(y,x) += CV_2PI;
                gap(y,x) += CV_2PI;
            }
        }
    }
    
    wrappedVectorMat.copyTo(previousVectorMat);
    
    return wrappedVectorMat;
}

@end

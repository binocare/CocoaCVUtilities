//
//  VADSMedianMeanSmoother.m
//  VADSDelegates
//
//  Created by JiangZhiping on 16/3/30.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "VADSMedianMeanSmoother.h"

@implementation VADSMedianMeanSmoother

+ (cv::Point2f) smoothingPoint:(cv::Point2f)aPoint ForKey:(NSString *)aStringKey TrajectoryLength:(int)trajectoryLength AndMedianLength:(int)trajectoryMedianLength{
    
    cv::Mat1d pointMat = CvMatMakeWithCvPoint(aPoint);
    cv::Mat1d smoothedPointMat = [VADSMedianMeanSmoother smoothingVector:pointMat ForKey:aStringKey TrajectoryLength:trajectoryLength AndMedianLength:trajectoryMedianLength];
    
    return cv::Point2f(smoothedPointMat(0),smoothedPointMat(1));
}


+ (cv::Mat1d) smoothingVector:(cv::Mat1d)aVectorMat ForKey:(NSString *)aStringKey TrajectoryLength:(int)trajectoryLength AndMedianLength:(int)trajectoryMedianLength{
    using namespace std;
    using namespace cv;
    static map<std::string,cv::Mat1d> keyArraysMap;
    static cv::Mat1d * currentMat;
    
    string keyString = aStringKey.UTF8String;
    
    if (keyArraysMap.count(keyString) == 0) {
        keyArraysMap[keyString] = cv::Mat1d(aVectorMat.size());
    }
    currentMat = & keyArraysMap[keyString];
    
    cv::hconcat(*currentMat, aVectorMat, *currentMat);
    if (currentMat->cols > trajectoryLength) {
        currentMat->colRange(1, currentMat->cols).copyTo(*currentMat);
    }
    
    cv::Mat1d sortResult;
    cv::sort(*currentMat, sortResult, CV_SORT_EVERY_ROW+CV_SORT_ASCENDING);
    if (sortResult.cols > trajectoryMedianLength) {
        sortResult.colRange((sortResult.cols-trajectoryMedianLength)/2, (sortResult.cols+trajectoryMedianLength)/2).copyTo(sortResult);
    }
    cv::reduce(sortResult, sortResult, 1, CV_REDUCE_AVG);
    return sortResult;
}


@end

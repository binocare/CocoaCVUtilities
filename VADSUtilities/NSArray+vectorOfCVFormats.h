//
//  NSArray+vectorOfCVFormats.h
//  CocoaCVUtilities
//
//  Created by JiangZhping on 16/7/30.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>
#import <vector>

using namespace cv;
using namespace std;

@interface NSArray (vectorOfCVFormats)

+ (NSArray<NSValue *> *) arrayWithCvPoint2iVector:(vector<Point2i>)v;
+ (NSArray<NSValue *> *) arrayWithCvPoint2fVector:(vector<Point2f>)v;
+ (NSArray<NSValue *> *) arrayWithCvPoint2dVector:(vector<Point2d>)v;
+ (NSArray<NSValue *> *) arrayWithCvPoint3iVector:(vector<Point3i>)v;
+ (NSArray<NSValue *> *) arrayWithCvPoint3fVector:(vector<Point3f>)v;
+ (NSArray<NSValue *> *) arrayWithCvPoint3dVector:(vector<Point3d>)v;
+ (NSArray<NSValue *> *) arrayWithCvSize2iVector:(vector<Size2i>)v;
+ (NSArray<NSValue *> *) arrayWithCvSize2fVector:(vector<Size2f>)v;
+ (NSArray<NSValue *> *) arrayWithCvSize2dVector:(vector<Size2d>)v;
+ (NSArray<NSValue *> *) arrayWithCvRect2iVector:(vector<Rect2i>)v;
+ (NSArray<NSValue *> *) arrayWithCvRect2fVector:(vector<Rect2f>)v;
+ (NSArray<NSValue *> *) arrayWithCvRect2dVector:(vector<Rect2d>)v;

- (vector<Point2i>) CvPoint2iVector;
- (vector<Point2f>) CvPoint2fVector;
- (vector<Point2d>) CvPoint2dVector;
- (vector<Point3i>) CvPoint3iVector;
- (vector<Point3f>) CvPoint3fVector;
- (vector<Point3d>) CvPoint3dVector;
- (vector<Size2i>) CvSize2iVector;
- (vector<Size2f>) CvSize2fVector;
- (vector<Size2d>) CvSize2dVector;
- (vector<Rect2i>) CvRect2iVector;
- (vector<Rect2f>) CvRect2fVector;
- (vector<Rect2d>) CvRect2dVector;

@end

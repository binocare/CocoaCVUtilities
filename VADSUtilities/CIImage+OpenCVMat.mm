//
//  CIImage+OpenCVMat.m
//  CocoaCVUtilities
//
//  Created by JiangZhping on 2016/9/28.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CIImage+OpenCVMat.h"

@implementation CIImage (OpenCVMat)

@dynamic cvMat;

+ (instancetype) imageWithCvMat:(const cv::Mat &)cvMat {
    CVPixelBufferRef pb = NULL;
    [CIImage convertCVMat:cvMat ToPixelBuffer:&pb];
    return [CIImage imageWithContent:(__bridge id)pb];
}

- (cv::Mat)cvMat {
    cv::Mat resultCVMat;
    [CIImage convertPixelBuffer:self.cvPixelBuffer ToCVMat:resultCVMat withMatClone:NO];
    return resultCVMat;
}

#pragma mark - Implementation

+ (BOOL) convertCVMat:(const cv::Mat &)aBGRACVMat ToPixelBuffer:(CVPixelBufferRef *)outputPixelBuffer{
    size_t imageWidth = aBGRACVMat.size().width;
    size_t imageHeight = aBGRACVMat.size().height;
    OSType pixelFormatType = kCVPixelFormatType_32BGRA;
    switch (aBGRACVMat.channels()) {
        case 1:
            pixelFormatType = kCVPixelFormatType_8Indexed;
        case 3:
            pixelFormatType = kCVPixelFormatType_24BGR;
            break;
        case 4:
            pixelFormatType = kCVPixelFormatType_32BGRA;
        default:
            break;
    }
    CVReturn cvRet = kCVReturnError;
    
    NSDictionary *options =
    @{(id)kCVPixelBufferCGImageCompatibilityKey: @YES,
     (id)kCVPixelBufferCGBitmapContextCompatibilityKey: @YES};
    
    cvRet = CVPixelBufferCreateWithBytes(kCFAllocatorDefault, imageWidth, imageHeight, pixelFormatType, aBGRACVMat.data, aBGRACVMat.step, nil, nil, (__bridge CFDictionaryRef)options, outputPixelBuffer);
    
    if (cvRet != kCVReturnSuccess) {
        [NSException raise:@"OpenCv Mat -> CVPixelBuffer Conversion failed." format:@""];
        return false;
    }
    
    return true;
}

+(void) convertPixelBuffer:(const CVPixelBufferRef)pixelBuffer ToCVMat:(cv::Mat &)resultCVMat withMatClone:(BOOL)aClone {
    
    size_t height = CVPixelBufferGetHeight( pixelBuffer );
    size_t width = CVPixelBufferGetWidth(pixelBuffer);
    size_t stride = CVPixelBufferGetBytesPerRow( pixelBuffer );
    size_t extendedWidth = stride / sizeof( uint32_t ); // each pixel is 4 bytes/32 bits
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    cv::Mat bgraImage = cv::Mat( (int)height, (int)extendedWidth, CV_8UC4, CVPixelBufferGetBaseAddress(pixelBuffer));
    if (width != extendedWidth) {
        bgraImage = bgraImage.colRange(0, (int)width);
    }
    
    if (aClone) {
        bgraImage.copyTo(resultCVMat);
    } else
        resultCVMat = bgraImage;
    
    CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );
}

#pragma mark - conversion shortcuts

+ (cv::Mat)conversionCvMatFromCGImage:(CGImageRef)cgImage {
    return [CIImage imageWithContent:(__bridge id)cgImage].cvMat;
}

+ (cv::Mat)conversionCvMatFromCVPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    return [CIImage imageWithContent:(__bridge id)pixelBuffer].cvMat;
}

+ (cv::Mat)conversionCvMatFromNSUIImage:(NSUIImage *)nsuiImage {
    return [CIImage imageWithContent:nsuiImage].cvMat;
}

+ (cv::Mat)conversionCvMatFromMTLTexture:(id<MTLTexture>)mtlTexture {
    return [CIImage imageWithContent:mtlTexture].cvMat;
}

+ (CGImageRef)conversionCGImageFromCvMat:(const cv::Mat &)cvMat {
    return [CIImage imageWithCvMat:cvMat].cgImage;
}

+ (CVPixelBufferRef)conversionCVPixelBufferFromCvMat:(const cv::Mat &)cvMat {
    return [CIImage imageWithCvMat:cvMat].cvPixelBuffer;
}

+ (NSUIImage *)conversionNSUIImageFromCvMat:(const cv::Mat &)cvMat {
    return [CIImage imageWithCvMat:cvMat].nsuiImage;
}

+ (id<MTLTexture>)conversionMTLTextureFromCvMat:(const cv::Mat &)cvMat {
    return [CIImage imageWithCvMat:cvMat].mtlTexture;
}

@end

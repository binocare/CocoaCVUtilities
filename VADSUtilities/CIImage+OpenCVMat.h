//
//  CIImage+OpenCVMat.h
//  CocoaCVUtilities
//
//  Created by JiangZhping on 2016/9/28.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <CoreImage/CoreImage.h>
#import <opencv2/opencv.hpp>
#import <CocoaXtensions/CocoaXtensions.h>

@interface CIImage (OpenCVMat)

@property (nonatomic, readonly) cv::Mat cvMat;

+ (instancetype) imageWithCvMat:(const cv::Mat &)cvMat;

+ (cv::Mat) conversionCvMatFromCGImage:(CGImageRef)cgImage;
+ (cv::Mat) conversionCvMatFromCVPixelBuffer:(CVPixelBufferRef)pixelBuffer;
+ (cv::Mat) conversionCvMatFromNSUIImage:(NSUIImage *)nsuiImage;
+ (cv::Mat) conversionCvMatFromMTLTexture:(id<MTLTexture>)mtlTexture;

+ (CGImageRef) conversionCGImageFromCvMat:(const cv::Mat &)cvMat;
+ (CVPixelBufferRef) conversionCVPixelBufferFromCvMat:(const cv::Mat &)cvMat;
+ (NSUIImage *) conversionNSUIImageFromCvMat:(const cv::Mat &)cvMat;
+ (id<MTLTexture>) conversionMTLTextureFromCvMat:(const cv::Mat &)cvMat;

@end

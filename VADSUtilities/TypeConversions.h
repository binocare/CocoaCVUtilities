//
//  ConversionsUsingTemplate.h
//  CocoaCVUtilities
//
//  Created by JiangZhping on 16/7/30.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import <opencv2/opencv.hpp>
#import <CoreOpenCVExtensions/CoreOpenCVExtensions.h>
#import "TargetConditionals.h"

#pragma mark - declarations
template<typename _Tp> CGPoint CGPointMakeWithCvPoint(const cv::Point_<_Tp> & point);
template<typename _Tp> CGPoint CGPointMakeWithCvVec(const cv::Vec<_Tp, 2> & vec);
template<typename _Tp> CGPoint CGPointMakeWithCvMatx(const cv::Matx<_Tp, 2,1> & matx);
template<typename _Tp> CGPoint CGPointMakeWithCvMatx(const cv::Matx<_Tp, 1,2> & matx);
template<typename _Tp> CGPoint CGPointMakeWithCvMat(const cv::Mat_<_Tp> & mat);
template<typename _Tp> CGSize CGSizeMakeWithCvSize(const cv::Size_<_Tp> & size);
template<typename _Tp> CGRect CGRectMakeWithCvRect(const cv::Rect_<_Tp> & rect);

template<typename _Tp> cv::Point_<_Tp> CvPointMakeWithCGPoint(const CGPoint & point);
template<typename _Tp> cv::Point3_<_Tp> CvPoint3MakeWithGLKVector3(const GLKVector3 & point);
template<typename _Tp> cv::Size_<_Tp> CvSizeMakeWithCGSize(const CGSize & size);
template<typename _Tp> cv::Rect_<_Tp> CvRectMakeWithCGRect(const CGRect & rect);

template<typename _Tp> GLKVector2 GLKVector2MakeWithCvPoint(const cv::Point_<_Tp> & point);
template<typename _Tp> GLKVector2 GLKVector2MakeWithCvVec2(const cv::Vec<_Tp, 2> & vec);
template<typename _Tp> GLKVector3 GLKVector3MakeWithCvPoint3(const cv::Point3_<_Tp> & point);
template<typename _Tp> GLKVector3 GLKVector3MakeWithCvVec3(const cv::Vec<_Tp, 3> & vec);
template<typename _Tp> GLKVector4 GLKVector4MakeWithCvVec4(const cv::Vec<_Tp, 4> & vec);
template<typename _Tp> GLKMatrix3 GLKMatrix3MakeWithCvMatx33(const cv::Matx<_Tp, 3, 3> & matx);
template<typename _Tp> GLKMatrix3 GLKMatrix3MakeWithCVMat(const cv::Mat_<_Tp> & mat);
template<typename _Tp> GLKMatrix4 GLKMatrix4MakeWithCvMatx44(const cv::Matx<_Tp, 4, 4> & matx);
template<typename _Tp> GLKMatrix4 GLKMatrix4MakeWithCvMat(const cv::Mat_<_Tp> & mat);

template<typename _Tp> cv::Vec<_Tp, 2> CvVecMakeWithGLKVector2(const GLKVector2 & vec);
template<typename _Tp> cv::Vec<_Tp, 3> CvVec3MakeWithGLKVector3(const GLKVector3 & vec);
template<typename _Tp> cv::Vec<_Tp, 4> CvVec4MakeWithGLKVector4(const GLKVector4 & vec);

template<typename _Tp> cv::Mat_<_Tp> CvMatMakeWithGLKMatrix3(const GLKMatrix3 & glkM);
template<typename _Tp> cv::Matx<_Tp, 3, 3> CvMatxMakeWithGLKMatrix3(const GLKMatrix3 & glkM);
template<typename _Tp> cv::Mat_<_Tp> CvMatMakeWithGLKMatrix4(const GLKMatrix4 & glkM);
template<typename _Tp> cv::Matx<_Tp, 4, 4> CvMatxMakeWithGLKMatrix4(const GLKMatrix4 & glkM);

#pragma mark - implementations

template<typename _Tp> CGPoint CGPointMakeWithCvPoint(const cv::Point_<_Tp> & point) {
    return CGPointMake(point.x, point.y);
}

template<typename _Tp> CGPoint CGPointMakeWithCvVec(const cv::Vec<_Tp, 2> & vec) {
    return CGPointMake(vec[0], vec[1]);
}

template<typename _Tp> CGPoint CGPointMakeWithCvMatx(const cv::Matx<_Tp, 2,1> & matx) {
    return CGPointMake(matx(0), matx(1));
}

template<typename _Tp> CGPoint CGPointMakeWithCvMatx(const cv::Matx<_Tp, 1,2> & matx) {
    return CGPointMake(matx(0), matx(1));
}

template<typename _Tp> CGPoint CGPointMakeWithCvMat(const cv::Mat_<_Tp> & mat) {
    return CGPointMake(mat.template at<_Tp>(0), mat.template at<_Tp>(1));
}

template<typename _Tp> CGSize CGSizeMakeWithCvSize(const cv::Size_<_Tp> & size) {
    return CGSizeMake(size.width, size.height);
}

template<typename _Tp>  CGRect CGRectMakeWithCvRect(const cv::Rect_<_Tp> & rect) {
    return CGRectMake(rect.x, rect.y, rect.width, rect.height);
}

template<typename _Tp> cv::Point_<_Tp> CvPointMakeWithCGPoint(const CGPoint & point) {
    return cv::Point_<_Tp>((_Tp)point.x, (_Tp)point.y);
}

template<typename _Tp> cv::Point3_<_Tp> CvPoint3MakeWithGLKVector3(const GLKVector3 & point) {
    return cv::Point3_<_Tp>((_Tp)point.x, (_Tp)point.y, (_Tp)point.z);
}

template<typename _Tp> cv::Size_<_Tp> CvSizeMakeWithCGSize(const CGSize & size) {
    return cv::Size_<_Tp>((_Tp)size.width, (_Tp)size.height);
}

template<typename _Tp> cv::Rect_<_Tp> CvRectMakeWithCGRect(const CGRect & rect) {
    return cv::Rect_<_Tp>((_Tp)rect.origin.x, (_Tp)rect.origin.y, (_Tp)rect.size.width, (_Tp)rect.size.height);
}

template<typename _Tp> GLKVector2 GLKVector2MakeWithCvPoint(const cv::Point_<_Tp> & point) {
    return GLKVector2Make(point.x, point.y);
}

template<typename _Tp> GLKVector2 GLKVector2MakeWithCvVec2(const cv::Vec<_Tp, 2> & vec) {
    return GLKVector2Make(vec[0], vec[1]);
}

template<typename _Tp> GLKVector3 GLKVector3MakeWithCvVec3(const cv::Vec<_Tp, 3> & vec) {
    return GLKVector3Make(vec[0], vec[1], vec[2]);
}

template<typename _Tp> GLKVector3 GLKVector3MakeWithCvPoint3(const cv::Point3_<_Tp> & point) {
    return GLKVector3Make(point.x, point.y, point.z);
}

template<typename _Tp> GLKVector4 GLKVector4MakeWithCvVec4(const cv::Vec<_Tp, 4> & vec) {
    return GLKVector3Make(vec[0], vec[1], vec[2], vec[3]);
}

template<typename _Tp> cv::Vec<_Tp, 2> CvVecMakeWithGLKVector2(const GLKVector2 & vec) {
    return cv::Vec<_Tp, 2>((_Tp)vec.x, (_Tp)vec.y);
}

template<typename _Tp> cv::Vec<_Tp, 3> CvVec3MakeWithGLKVector3(const GLKVector3 & vec) {
    return cv::Vec<_Tp, 3>((_Tp)vec.x, (_Tp)vec.y, (_Tp)vec.z);
}

template<typename _Tp> cv::Vec<_Tp, 4> CvVec4MakeWithGLKVector4(const GLKVector4 & vec) {
    return cv::Vec<_Tp, 4>((_Tp)vec.x, (_Tp)vec.y, (_Tp)vec.z, (_Tp)vec.w);
}

template<typename _Tp> GLKMatrix3 GLKMatrix3MakeWithCvMatx33(const cv::Matx<_Tp, 3, 3> & matx) {
    return GLKMatrix3Make(matx(0,0), matx(1,0), matx(2,0), matx(0,1), matx(1,1), matx(2,1), matx(0,2), matx(1,2), matx(2,2));
}

template<typename _Tp> GLKMatrix4 GLKMatrix4MakeWithCvMatx44(const cv::Matx<_Tp, 4, 4> & matx) {
    return GLKMatrix4Make(matx(0,0), matx(1,0), matx(2,0), matx(3,0), matx(0,1), matx(1,1), matx(2,1), matx(3,1), matx(0,2), matx(1,2), matx(2,2), matx(3,2), matx(0,3), matx(1,3), matx(2,3), matx(3,3));
}

template<typename _Tp> GLKMatrix3 GLKMatrix3MakeWithCVMat(const cv::Mat_<_Tp> & matx) {
    if (matx.rows >= 3 && matx.cols >= 3)
        return GLKMatrix3Make(matx.template at<_Tp>(0,0), matx.template at<_Tp>(1,0), matx.template at<_Tp>(2,0), matx.template at<_Tp>(0,1), matx.template at<_Tp>(1,1), matx.template at<_Tp>(2,1), matx.template at<_Tp>(0,2), matx.template at<_Tp>(1,2), matx.template at<_Tp>(2,2));
    else
        return GLKMatrix3Identity;
}

template<typename _Tp> GLKMatrix4 GLKMatrix4MakeWithCvMat(const cv::Mat_<_Tp> & matx) {
    if (matx.rows >= 4 && matx.cols >= 4)
        return GLKMatrix4Make(matx.template at<_Tp>(0,0), matx.template at<_Tp>(1,0), matx.template at<_Tp>(2,0), matx.template at<_Tp>(3,0), matx.template at<_Tp>(0,1), matx.template at<_Tp>(1,1), matx.template at<_Tp>(2,1), matx.template at<_Tp>(3,1), matx.template at<_Tp>(0,2), matx.template at<_Tp>(1,2), matx.template at<_Tp>(2,2), matx.template at<_Tp>(3,2), matx.template at<_Tp>(0,3), matx.template at<_Tp>(1,3), matx.template at<_Tp>(2,3), matx.template at<_Tp>(3,3));
    else
        return GLKMatrix4Identity;
}

template<typename _Tp> cv::Mat_<_Tp> CvMatMakeWithGLKMatrix3(const GLKMatrix3 & glkM) {
    return cv::Mat_<_Tp>(3,3) << (_Tp)glkM.m00, (_Tp)glkM.m10, (_Tp)glkM.m20, (_Tp)glkM.m01, (_Tp)glkM.m11, (_Tp)glkM.m21, (_Tp)glkM.m02, (_Tp)glkM.m12, (_Tp)glkM.m22;
}

template<typename _Tp> cv::Matx<_Tp, 3, 3> CvMatxMakeWithGLKMatrix3(const GLKMatrix3 & glkM) {
    return cv::Matx<_Tp,3,3> ((_Tp)glkM.m00, (_Tp)glkM.m10, (_Tp)glkM.m20, (_Tp)glkM.m01, (_Tp)glkM.m11, (_Tp)glkM.m21, (_Tp)glkM.m02, (_Tp)glkM.m12, (_Tp)glkM.m22);
}

template<typename _Tp> cv::Mat_<_Tp> CvMatMakeWithGLKMatrix4(const GLKMatrix4 & glkM) {
    return cv::Mat_<_Tp>(4,4) << (_Tp)glkM.m00, (_Tp)glkM.m10, (_Tp)glkM.m20, (_Tp)glkM.m30, (_Tp)glkM.m01, (_Tp)glkM.m11, (_Tp)glkM.m21, (_Tp)glkM.m31, (_Tp)glkM.m02, (_Tp)glkM.m12, (_Tp)glkM.m22, (_Tp)glkM.m32, (_Tp)glkM.m03, (_Tp)glkM.m13, (_Tp)glkM.m23, (_Tp)glkM.m33;
}

template<typename _Tp> cv::Matx<_Tp, 4, 4> CvMatxMakeWithGLKMatrix4(const GLKMatrix4 & glkM) {
    return cv::Matx<_Tp,4,4> ((_Tp)glkM.m00, (_Tp)glkM.m10, (_Tp)glkM.m20, (_Tp)glkM.m30, (_Tp)glkM.m01, (_Tp)glkM.m11, (_Tp)glkM.m21, (_Tp)glkM.m31, (_Tp)glkM.m02, (_Tp)glkM.m12, (_Tp)glkM.m22, (_Tp)glkM.m32, (_Tp)glkM.m03, (_Tp)glkM.m13, (_Tp)glkM.m23, (_Tp)glkM.m33);
}

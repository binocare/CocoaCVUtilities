//
//  NSValue+CVFormats.h
//  OpenFace
//
//  Created by JiangZhping on 16/7/30.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <SceneKit/SceneKit.h>
#import "TypeConversions.h"


@interface NSValue (CVFormats)

+ (NSValue *) valueWithCvRect2d:(cv::Rect2d)cvRect;
+ (NSValue *) valueWithCvPoint2d:(cv::Point2d)cvPoint;
+ (NSValue *) valueWithCvPoint3d:(cv::Point3d)cvPoint;
+ (NSValue *) valueWithCvSize2d:(cv::Size2d)cvSize;

- (cv::Rect2i) cvRect2iValue;
- (cv::Rect2f) cvRect2fValue;
- (cv::Rect2d) cvRect2dValue;
- (cv::Point2i) cvPoint2iValue;
- (cv::Point2f) cvPoint2fValue;
- (cv::Point2d) cvPoint2dValue;
- (cv::Point3i) cvPoint3iValue;
- (cv::Point3f) cvPoint3fValue;
- (cv::Point3d) cvPoint3dValue;
- (cv::Size2i) cvSize2iValue;
- (cv::Size2f) cvSize2fValue;
- (cv::Size2d) cvSize2dValue;
@end

//
//  ViewController.m
//  CameraCalibrator
//
//  Created by JiangZhping on 2016/10/18.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "ViewController.h"
#import <CocoaCVUtilities/CocoaCVUtilities.h>

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.gridWidth = 11;
    self.gridHeight = 8;
    self.gridSize = 30.f;
}

- (IBAction)calibrate:(id)sender {
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self calibrationTask];
//    });
}

- (void) calibrationTask {
    NSArray<NSString *> * imagePaths = self.fileDragView.imageFilePaths;
    std::vector<std::string> stdpaths;
    for (NSString * path in imagePaths) {
        stdpaths.push_back([path UTF8String]);
    }
    
    cv::Mat1d cameraMatrix;
    cv::Mat1d distCoeffs;
    BOOL success = cve::chessboardCameraCalibration((int)self.gridWidth, (int)self.gridHeight, self.gridSize, stdpaths, cameraMatrix, distCoeffs, NO);
    
    if (success == YES) {
        NSString * tempPath = [[NSFileManager defaultManager] createTemporaryFileWithExtension:@"yaml"];
        cv::FileStorage fs([tempPath UTF8String], cv::FileStorage::WRITE);
        fs <<"cameraMatrix"<<cameraMatrix<<"distCoeffs"<<distCoeffs;
        fs.release();
        
        NSSavePanel * savePanel = [NSSavePanel savePanel];
        savePanel.title = @"Save camera profile to (.yaml) file...";
        savePanel.nameFieldStringValue = @"CM_Camera.yaml";
        savePanel.prompt = @"Save Profile";
        [savePanel beginWithCompletionHandler:^(NSInteger result) {
            if (result == NSFileHandlingPanelOKButton) {
                NSString *path2Save = [[savePanel URL] path];
                [[NSFileManager defaultManager] moveItemAtPath:tempPath toPath:path2Save error:nil];
            }
        }];
    }
    
}


@end

//
//  ViewController.h
//  CameraCalibrator
//
//  Created by JiangZhping on 2016/10/18.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CameraCalibratorView.h"

@interface ViewController : NSViewController


@property (nonatomic) NSUInteger gridWidth;
@property (nonatomic) NSUInteger gridHeight;
@property (nonatomic) float gridSize;

@property (weak) IBOutlet CameraCalibratorView *fileDragView;

- (IBAction)calibrate:(id)sender;


@end

